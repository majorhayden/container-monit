#!/bin/bash
set -euxo pipefail

echo "The home directory is: ${HOME}"

# Work around OpenShift's arbitrary UID/GIDs.
if [ -w '/etc/passwd' ]; then
    echo "monit:x:`id -u`:`id -g`:,,,:${HOME}:/bin/bash" >> /etc/passwd
fi
if [ -w '/etc/group' ]; then
    echo "monit:x:$(id -G | cut -d' ' -f 2)" >> /etc/group
fi

# Make a basic monitrc.
echo "set daemon 30" >> "${HOME}"/monitrc
echo "include /config/*" >> "${HOME}"/monitrc
chmod 0700 "${HOME}"/monitrc

# Ensure the UID/GID mapping works.
id

# Run monit.
/usr/local/bin/monit -v -I -c "${HOME}"/monitrc
